cmd_gitea_help() {
    cat <<_EOF
    gitea [<command> <options>]
        Run 'gitea' inside the container.

_EOF
}

cmd_gitea() {
    ds shell gitea "$@"
}

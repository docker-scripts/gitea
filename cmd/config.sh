cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    check_settings

    # create the database
    [[ -n $DBHOST ]] && ds mariadb create

    ds inject gitea.sh
    ds inject setup-nginx.sh
}

check_settings() {
    [[ $DOMAIN == 'gitea.example.org' ]] && return 0

    [[ $ADMIN_USER == 'admin' || $ADMIN_USER == 'user' ]] &&\
        fail "Error: ADMIN_USER cannot be '$ADMIN_USER' because this is reserved by Gitea."
    [[ $ADMIN_PASS == '123456' ]] &&\
        fail "Error: ADMIN_PASS on 'settings.sh' has to be changed for security reasons."
    [[ -n $DBHOST && $DBPASS == '123456' ]] &&\
        fail "Error: DBPASS on 'settings.sh' should be changed for security reasons."
}

cmd_backup_help() {
    cat <<_EOF
    backup
        Make a backup.

_EOF
}

cmd_backup() {
    # backup file
    local backup="backup-$(date +%Y%m%d).zip"
    rm -rf $backup

    # make the backup
    ds exec rm -f /home/git/$backup
    ds exec su - git -c "gitea dump -c /etc/gitea/app.ini -f $backup"
    ds exec mv /home/git/$backup /host/
}

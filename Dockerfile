include(bookworm)

RUN <<EOF
  # update and upgrade
  apt update
  apt upgrade --yes

  # install some tools
  apt install --yes git wget

  # install nginx
  apt install --yes nginx ssl-cert
EOF

RUN <<EOF
  # download gitea
  wget -q -O /usr/local/bin/gitea \
      https://dl.gitea.com/gitea/1.22.6/gitea-1.22.6-linux-amd64
  chmod +x /usr/local/bin/gitea
EOF

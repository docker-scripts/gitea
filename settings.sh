APP=gitea
IMAGE=dockerscripts/gitea

#######################################################################

### If installing without revproxy, uncomment PORTS
### and comment DOMAIN
#PORTS="443:443"

### If you are installing on a LAN, use the domain 'hostname.local'
### instead, where 'hostname' is the name of the local host (as
### reported by the command `hostname`). In debian/ubuntu this needs
### the package 'avahi-daemon' to work.
DOMAIN=gitea.example.org
#DOMAIN=gitea.local

#######################################################################

### Application name, used in the page title.
APP_NAME="Gitea: Git with a cup of tea"

### Create an admin user. It should be different from "admin" because this
### is a reserved named by Gitea.
ADMIN_USER="username"
ADMIN_PASS="123456"
ADMIN_EMAIL="username@example.org"

#######################################################################

### Uncomment DB settings to use a mysql database. Change DBHOST if needed.
### You must have a server running MySQL, for example install 'mariadb'
### container with docker-scripts: https://gitlab.com/docker-scripts/mariadb
#DBHOST=mariadb    # this is the name of the mariadb container
#DBPORT=3306
#DBNAME=gitea_example_org
#DBUSER=gitea_example_org
#DBPASS=123456

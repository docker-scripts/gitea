#!/bin/bash -x
### Install gitea:
### https://docs.gitea.io/nl-nl/install-from-binary/

# load global settings
global_settings=$(dirname $0)/global_settings.sh
[[ -f $global_settings ]] && source $global_settings

# load local settings
source /host/settings.sh

main() {
    create_user
    create_directories
    create_gitea_service
    create_config_file
    create_admin_user
}

# create user to run gitea
create_user() {
    adduser \
        --system \
        --shell /bin/bash \
        --gecos 'Git Version Control' \
        --group \
        --disabled-password \
        --home /home/git \
        git
}

# create required directory structure
create_directories() {
    mkdir -p /var/lib/gitea/{custom,data,log}
    chown -R git:git /var/lib/gitea/
    chmod -R 750 /var/lib/gitea/
    mkdir -p /etc/gitea
    chown root:git /etc/gitea
    chmod 770 /etc/gitea
}

# create service for starting gitea
create_gitea_service() {
    cat <<EOF > /etc/systemd/system/gitea.service
[Unit]
Description=Gitea (Git with a cup of tea)
After=syslog.target
After=network.target
#Requires=mysql.service
#Requires=mariadb.service
#Requires=postgresql.service
#Requires=memcached.service
#Requires=redis.service

[Service]
# Modify these two values and uncomment them if you have
# repos with lots of files and get an HTTP error 500 because
# of that
###
#LimitMEMLOCK=infinity
#LimitNOFILE=65535
RestartSec=2s
Type=simple
User=git
Group=git
WorkingDirectory=/var/lib/gitea/
# If using unix socket: Tells Systemd to create /run/gitea folder to home gitea.sock
# Manual cration would vanish after reboot.
#RuntimeDirectory=gitea
ExecStart=/usr/local/bin/gitea web -c /etc/gitea/app.ini
Restart=always
Environment=USER=git HOME=/home/git GITEA_WORK_DIR=/var/lib/gitea
# If you want to bind Gitea to a port below 1024 uncomment
# the two values below
###
#CapabilityBoundingSet=CAP_NET_BIND_SERVICE
#AmbientCapabilities=CAP_NET_BIND_SERVICE

[Install]
WantedBy=multi-user.target
EOF

    # enable and start this service
    systemctl enable gitea
    systemctl start gitea
}

# create app.ini
create_config_file() {
    [[ -f /etc/gitea/app.ini ]] && return

    cat <<EOF > /etc/gitea/app.ini
APP_NAME = ${APP_NAME:-Gitea: Git with a cup of tea}
RUN_USER = git
RUN_MODE = prod

[oauth2]
JWT_SECRET = $(gitea generate secret JWT_SECRET)

[security]
INTERNAL_TOKEN = $(gitea generate secret INTERNAL_TOKEN)
SECRET_KEY     = $(gitea generate secret SECRET_KEY)
INSTALL_LOCK   = true

[repository]
ROOT = /var/lib/gitea/repositories
ANSI_CHARSET = utf8
DEFAULT_CLOSE_ISSUES_VIA_COMMITS_IN_ANY_BRANCH = true

[server]
DOMAIN           = $DOMAIN
HTTP_PORT        = 3000
ROOT_URL         = https://$DOMAIN/
DISABLE_SSH      = true
LFS_START_SERVER = true
LFS_JWT_SECRET   = $(gitea generate secret JWT_SECRET)
OFFLINE_MODE     = false

[lfs]
PATH = /var/lib/gitea/data/lfs

[service]
REGISTER_EMAIL_CONFIRM            = true
ENABLE_NOTIFY_MAIL                = true
DISABLE_REGISTRATION              = true
ALLOW_ONLY_EXTERNAL_REGISTRATION  = false
ENABLE_CAPTCHA                    = true
REQUIRE_SIGNIN_VIEW               = false
DEFAULT_KEEP_EMAIL_PRIVATE        = true
DEFAULT_ALLOW_CREATE_ORGANIZATION = true
DEFAULT_ENABLE_TIMETRACKING       = true
NO_REPLY_ADDRESS                  = noreply@${ADMIN_EMAIL##*@}

[picture]
DISABLE_GRAVATAR        = false
ENABLE_FEDERATED_AVATAR = true

[openid]
ENABLE_OPENID_SIGNIN = true
ENABLE_OPENID_SIGNUP = true

[session]
PROVIDER = file

[log]
MODE      = file
LEVEL     = info
ROOT_PATH = /var/lib/gitea/log

[mailer]
ENABLED        = true
FROM           = $ADMIN_EMAIL
MAILER_TYPE    = smtp
SMTP_ADDR      = $SMTP_SERVER
SMTP_PORT      = 587
#IS_TLS_ENABLED = true
#USER           = $ADMIN_EMAIL
#PASSWD         = password

ENABLED = true
MAILER_TYPE = sendmail

EOF

    # database settings
    if [[ -z $DBHOST ]]; then
        cat <<EOF >> /etc/gitea/app.ini
[database]
DB_TYPE  = sqlite3
PATH     = /var/lib/gitea/data/gitea.db
EOF
    else
        cat <<EOF >> /etc/gitea/app.ini
[database]
DB_TYPE  = mysql
HOST     = $DBHOST:${DBPORT:-3306}
NAME     = ${DBNAME:-gitea}
USER     = ${DBUSER:-gitea}
PASSWD   = ${DBPASS:-gitea}
EOF
    fi

    chown git: /etc/gitea/app.ini
    systemctl restart gitea
    sleep 10
}

create_admin_user() {
    su git -c "\
        gitea admin user create \
            --config /etc/gitea/app.ini \
            --username $ADMIN_USER \
            --password '$ADMIN_PASS' \
            --email '$ADMIN_EMAIL' \
            --admin"
}

# start configuration
main

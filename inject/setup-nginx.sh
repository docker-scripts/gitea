#!/bin/bash -x

# based on: https://docs.gitea.io/en-us/administration/reverse-proxies/#nginx
ip=$(hostname -I)
network=${ip%.*}.0/16
cat <<EOF > /etc/nginx/sites-available/default
# vim: syntax=nginx
server {
    listen 443 ssl;
    server_name _;

    ssl_certificate /etc/ssl/certs/ssl-cert-snakeoil.pem;
    ssl_certificate_key /etc/ssl/private/ssl-cert-snakeoil.key;

    real_ip_header X-Forwarded-For;
    set_real_ip_from $network;

    location / {
        proxy_pass http://localhost:3000;
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
    }
}
EOF

systemctl reload nginx

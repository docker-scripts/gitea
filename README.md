# GiTea container

[Gitea](https://gitea.io) (Git with a cup of tea) is a painless
self-hosted Git service.

## Installation

  - First install `ds` and `revproxy`:
      + https://gitlab.com/docker-scripts/ds#installation
      + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull gitea`

  - Create a directory for the container: `ds init gitea @gitea.example.org`

  - Fix the settings: `cd /var/ds/gitea.example.org/ ; vim settings.sh`

  - Build image, create the container and configure it: `ds make`

## Other commands

```
ds gitea

ds stop
ds start
ds shell
ds help
```
